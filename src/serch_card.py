import utils.autres as autres
import sys

def main(argv) :
    if len(argv) >= 1:
        selected_cards = autres.select_cards_by_name(argv[0])
        print("Carte pour : " + argv[0] + ' :')
        for i in range (len(selected_cards)) :
            print('\t' + str(i) +  " : " +  selected_cards[i])
    else :
        print("Il faut en paramètre un nom")


if __name__ == "__main__":
    main(sys.argv[1:])
