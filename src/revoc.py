import utils.autres as autres
import utils.crypt as crypt
import utils.pathlist as path
from getpass import getpass
import sys

def revoc(id) :
    reps = autres.select_all()
    for rep in reps :
        if rep[0] != id :
            auth_not_finish = True
            tent = 0
            while auth_not_finish :
                login = input("Login for id " + str(rep[0]) + ' : ')
                if autres.check_login(rep[0], login) :
                    password = crypt.encrypt_data(getpass("Password : "), path.KEY_PASSWORD)
                    if autres.check_password(rep[0], password) :
                        auth_not_finish = False
                tent = tent + 1
                #On laisse 3 tentative pour donner le bon mot de passe
                if tent >= 3 :
                    sys.exit("Authentication raté nombre de tentative surpasé")
    autres.delete_manager(id)
    print("======== Revocation réussite ========")

def main(argv) :
    if len(argv) >= 1 :
        revoc(argv[0])
    else :
        print("Il faut en paramètre l'ID de la personne a révoquer")


if __name__ == "__main__":
    main(sys.argv[1:])
