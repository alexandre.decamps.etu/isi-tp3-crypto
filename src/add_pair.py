import utils.autres as autres
import sys

def main(argv) :
    if len(argv) >= 2:
        autres.insert_pair(argv[0], argv[1])
    else :
        print("Il faut un nom suivit d'un numéro de carte")

if __name__ == "__main__":
    main(sys.argv[1:])
