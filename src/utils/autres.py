import utils.crypt as crypt
import utils.pathlist as path
from getpass import getpass
import sys

#=============GESTION DES CARTES =============

def insert_pair(name, num_card) :
    with open(path.DATABASE_CARD, "a") as f :
        f.write(name + ";" + crypt.encrypt_data(num_card, path.MASTER_KEY).decode() + ';\n')

def delete_pair(name, num_card) :
    with open(path.DATABASE_CARD, "r") as f:
        lines = f.readlines()
    with open(path.DATABASE_CARD, "w") as f:
        for line in lines:
            splitted_line = line.split(';')
            if name in splitted_line :
                card = crypt.decrypt_data(splitted_line[1].encode(), path.MASTER_KEY)
                if card != num_card :
                    f.write(line)
            else  :
                f.write(line)

def select_cards_by_name(name) :
    with open(path.DATABASE_CARD, "r") as f:
        cards = []
        for line in f:
            splitted_line = line.split(';')
            if name in splitted_line:
                cards.append(crypt.decrypt_data(splitted_line[1].encode(), path.MASTER_KEY))
        return cards

#=============GESTION DES RESPONSABLE/MANAGERS=============

def insert_manager(id, login, crypted_password, role) :
    if role != "legal" and role != "technical" :
        sys.exit("Ce doit etre un manager legal ou technical")
    db_managers = select_all()
    for manager in db_managers :
        if manager[0] == str(id) :
            sys.exit("L'Id doit être unique")
        if manager[1] == login :
            sys.exit("Le login doit être unique")
    to_write = str(id) + ';' + login + ';' + crypted_password.decode()  + ';' + role + ';' + '\n'
    with open(path.DATABASE_MANAGERS, "a") as f :
        f.write(to_write)

def delete_manager(id) :
    db_managers = select_all()
    with open(path.DATABASE_MANAGERS, "w") as f :
        for managers in db_managers :
            if managers[0] != id :
                to_write = managers[0] + ';' + managers[1] + ';' + managers[2]  + ';' + managers[3] + ';' + '\n'
                f.write(to_write)

def select_managers_by_id(id) :
    with open(path.DATABASE_MANAGERS, "r") as f :
        for line in f :
            splitted_line = line.split(';')
            if splitted_line[0] == str(id) :
                return splitted_line

def select_managers_by_login(login) :
    with open(path.DATABASE_MANAGERS, "r") as f :
        for line in f :
            splitted_line = line.split(';')
            if splitted_line[1] == login :
                return splitted_line

def select_managers_by_role(role) :
    selected_managers = []
    with open(path.DATABASE_MANAGERS, "r") as f :
        for line in f :
            splitted_line = line.split(';')
            if splitted_line[3] == role :
                selected_managers.append(splitted_line)
    return splitted_line

def select_all() :
    db_managers = []
    with open(path.DATABASE_MANAGERS, "r") as f :
        for line in f :
            splitted_line = line.split(';')
            db_managers.append(splitted_line)
    return db_managers

def check_password(id, crypted_password) :
    manager = select_managers_by_id(id)
    if crypt.decrypt_data(manager[2].encode(), path.KEY_PASSWORD) == crypt.decrypt_data(crypted_password, path.KEY_PASSWORD):
        return True
    else :
        print("Mauvais mot de passe")
        return False

def check_login(id, login) :
    manager = select_managers_by_id(id)
    if manager[1] == login :
        return True
    else :
        print("Mauvais login")
        return False

def check_id_rights(id) :
    managers = select_all()
    for manager in managers :
        if manager[0] == str(id) :
            return True
    print("Cet id ne corespond à aucun responsable autoriser")
    return False

def get_manager_role(id) :
    with open(path.DATABASE_MANAGERS, "r") as f :
        for line in f :
            splitted_line = line.split(';')
            if splitted_line[0] == str(id) :
                return splitted_line[3]
