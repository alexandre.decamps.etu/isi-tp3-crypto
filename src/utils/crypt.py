from cryptography.fernet import Fernet
import sys

def generate_key() :
    key = Fernet.generate_key()
    return key

def write_key(key, filepath) :
    with open(filepath, "wb") as key_file:
        key_file.write(key)

def load_key(filepath) :
    try :
         return open(filepath, "rb").read()
    except FileNotFoundError :
        sys.exit("La clé n'a pas été trouvé")

def encrypt_data(data, filepath) :
    key = load_key(filepath)
    if type(data) != bytes :
        data = data.encode()
    f = Fernet(key)
    encrypted_data = f.encrypt(data)
    return encrypted_data

def encrypt_data_with_key(data, key) :
    f = Fernet(key)
    if type(data) != bytes :
        data = data.encode()
    encrypted_data = f.encrypt(data)
    return encrypted_data

def decrypt_data(data, filepath) :
    key = load_key(filepath)
    f = Fernet(key)
    decrypted_data = f.decrypt(data)
    return (decrypted_data.decode())

def decrypt_data_with_key(data, key) :
    f = Fernet(key)
    decrypted_data = f.decrypt(data)
    return (decrypted_data.decode())
