import pathlib

ROOT_PROJECT = str(pathlib.Path(__file__).parents[2])
MASTER_KEY =  ROOT_PROJECT  + "/ramdisk/keys/MASTER_KEY.key"
SERVER_KEYS_DIR =  ROOT_PROJECT  + "/disques/server/keys/"
KEY_RESP =  ROOT_PROJECT + "/disques/server/keys/KEY_RESP.key"
KEY_PASSWORD = ROOT_PROJECT + "/disques/server/keys/KEY_PASSWORD.key"
USB0 = ROOT_PROJECT + "/disques/usb0/"
USB1 = ROOT_PROJECT + "/disques/usb1/"
DATABASE_CARD = ROOT_PROJECT + "/disques/server/data/card"
DATABASE_MANAGERS = ROOT_PROJECT + "/disques/server/data/managers"
NB_USB = ROOT_PROJECT + "/disques/server/data/nb_usb"

def get_path_usb_id(path_usb) :
    return path_usb + "/.id"

def get_path_usb_key(path_usb) :
    return path_usb + "/.KEY.key"

def get_path_crypted_master_key(usb0, usb1) :
    if usb0 < usb1 :
        str_id = str(usb0) + '_' + str(usb1)
    else :
        str_id = str(usb1) + '_' + str(usb0)
    return SERVER_KEYS_DIR +  "CRYPTED_MASTER_KEY_" + str_id + ".key"
