import utils.crypt as crypt
import utils.pathlist as path
import utils.autres as autres
from getpass import getpass
import sys


def read_nb_usb() :
    try :
        with open(path.NB_USB, "r+") as f :
            return int(f.read())
    except FileNotFoundError :
        print("Le fichier n'a pas été trouvé")

def update_nb_usb(new_n) :
    with open(path.NB_USB, 'w') as f :
        f.write(str(new_n))

def set_id(id, path_usb) :
    path_rank = path.get_path_usb_id(path_usb)
    with open(path_rank, "w+") as f :
        f.write(str(id))

def get_id(path_usb) :
    path_rank = path.get_path_usb_id(path_usb)
    try :
        with open(path_rank, "r") as f :
            return int(f.read())
    except FileNotFoundError :
        print("Cet USB n'a pas été init")

def create_password() :
    return crypt.encrypt_data(getpass("Password : "), path.KEY_PASSWORD)

def create_login() :
    return input("login : ")

def read_key(path_usb) :
    path_key = path.get_path_usb_key(path_usb)
    with open(path_key, "rb") as f :
        return f.read()

def init_usb(path_usb, role) :
    id = read_nb_usb()
    set_id(id, path_usb)
    login = create_login()
    password = create_password()
    autres.insert_manager(id, login, password, role)
    update_nb_usb(read_nb_usb() + 1)
    return id
