import sys
import utils.crypt as crypt
import utils.pathlist as path
import utils.usb as usb
import utils.autres as autres
from getpass import getpass

def check_roles(id1, id2) :
    if autres.check_id_rights(id1) and autres.check_id_rights(id2) :
        role1 = autres.get_manager_role(id1)
        role2 = autres.get_manager_role(id2)
        if role1 != role2 :
            if (role1 == "legal" or role2 == "legal") and (role1 == "technical" or role2 == "technical") :
                return True
        print("Il faut un représentant légal et technique")
    return False

def authentification(id) :
    attempt = 0
    while attempt < 3 :
        input_login = input("Login pour le représentant avec l'id " + str(id) + ' : ')
        if autres.check_login(id, input_login) :
            input_password = crypt.encrypt_data(getpass("Password : "), path.KEY_PASSWORD)
            if autres.check_password(id, input_password) :
                return True
        attempt = attempt + 1
    print("Trop de mauvaise tentative pour l'usb" + str(id))
    return False

def load_master_key(usb_path1, usb_path2, path_crypted_mk) :
    master_key = crypt.load_key(path_crypted_mk)

    # On décrypt les clés des clés usbs
    decrypted_usb_key1 = crypt.decrypt_data(usb.read_key(usb_path1), path.KEY_RESP).encode()
    decrypted_usb_key2 = crypt.decrypt_data(usb.read_key(usb_path2), path.KEY_RESP).encode()

    # On ouble decrypt la MASTER_KEY (celui qui a l'id le plus grand décrypt en 1er)
    if usb.get_id(usb_path1) > usb.get_id(usb_path2) :
        master_key = crypt.decrypt_data_with_key(master_key, decrypted_usb_key1)
        master_key = crypt.decrypt_data_with_key(master_key.encode(), decrypted_usb_key2)
    else :
        master_key = crypt.decrypt_data_with_key(master_key, decrypted_usb_key2)
        master_key = crypt.decrypt_data_with_key(master_key.encode(), decrypted_usb_key1)
    crypt.write_key(master_key.encode(), path.MASTER_KEY)

def main(argv) :
    if len(argv) >= 2:
        print("=========== Lancement du serveur ===========")
        #On récup les ID
        id1 = usb.get_id(argv[0])
        id2 = usb.get_id(argv[1])
        if check_roles(id1, id2) : #On vérifie bien que l'on a un représentant technique ET légal.
            if authentification(id1) and authentification(id2) :
                print("=========== Authentification réussite ===========")
                path_crypted_mk = path.get_path_crypted_master_key(id1, id2)
                load_master_key(argv[0], argv[1], path_crypted_mk)
                print("=========== La MASTER_KEY est chargé dans le ramdisk ===========")
    else :
        print("Il faut en paramètre deux chemins vers des clés USB")

if __name__ == "__main__":
    main(sys.argv[1:])
