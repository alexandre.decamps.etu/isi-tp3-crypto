import os
import sys
import utils.usb as usb
import utils.pathlist as path
import utils.crypt as crypt

def generate_all(usb_path_new, role, id, usb_paths) :
    for usb_path in usb_paths :
        if os.path.exists(usb_path) :
            # chemin vers la nouvelle master key cryptée
            keypath = path.get_path_crypted_master_key(usb.get_id(usb_path), id)

            master_key = crypt.load_key(path.MASTER_KEY)
            decrypt_manager_key = crypt.decrypt_data(usb.read_key(usb_path), path.KEY_RESP).encode()
            key_new = crypt.generate_key()
            # On double crypt la MASTER_KEY (on fait dans l'ordre de l'id le plus petit au plus grand)
            if usb.get_id(usb_path) < id :
                master_key = crypt.encrypt_data_with_key(master_key, decrypt_manager_key)
                master_key = crypt.encrypt_data_with_key(master_key, key_new)
            else :
                master_key = crypt.encrypt_data_with_key(master_key, key_new)
                master_key = crypt.encrypt_data_with_key(master_key, decrypt_manager_key)
            crypt.write_key(master_key, keypath)
            crypt.write_key(crypt.encrypt_data(key_new, path.KEY_RESP), path.get_path_usb_key(usb_path_new))

def main(argv) :
    if len(argv) >= 3:
        print("===== Ajout d'un nouveau responsable légal =====")
        id = usb.init_usb(argv[0], argv[1])
        generate_all(argv[0], argv[1], id, argv[2:])
    else :
        print("Il faut en paramètres Le chemin vers la clé du nouveau responsable, son role")
        print("ensuite les chemins vers les clés usb qui permettras de lancer le serveur en association")


if __name__ == "__main__":
    main(sys.argv[1:])
