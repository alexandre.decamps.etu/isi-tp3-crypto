import os
import utils.pathlist as path
import utils.crypt as crypt
import utils.usb as usb
import utils.autres as autres

def main() :
    print("=====Initialisation du serveur=====")
    with open(path.DATABASE_MANAGERS, "w") as f :
        f.write('')
    with open(path.DATABASE_CARD, "w") as f :
        f.write('')

    password_key = crypt.generate_key()
    crypt.write_key(password_key, path.KEY_RESP)

    # On init les clés usb
    usb.update_nb_usb(0)
    print("")
    print("========== Initialisation du Responsable Juridique ==========")
    usb.init_usb(path.USB0, "legal")
    print("========== Initialisation du Responsable Technique ======")
    usb.init_usb(path.USB1, "technical")

    # On crée les clés
    key_usb0 = crypt.generate_key()
    key_usb1 = crypt.generate_key()
    master_key = crypt.generate_key()

    # On double crypt la MASTER_KEY (on fait dans l'ordre de l'id le plus petit au plus grand)
    master_key = crypt.encrypt_data_with_key(master_key, key_usb0)
    master_key = crypt.encrypt_data_with_key(master_key, key_usb1)

    # On crypte les clés des responsable puis les sauvegardes
    crypt.write_key(crypt.encrypt_data(key_usb0, path.KEY_RESP), path.get_path_usb_key(path.USB0))
    crypt.write_key(crypt.encrypt_data(key_usb1, path.KEY_RESP), path.get_path_usb_key(path.USB1))

    # On recupere le chemin ou va être stocker la MASTER_KEY cryptée et la sauvegarde
    path_crypted_mk = path.get_path_crypted_master_key(usb.get_id(path.USB0), usb.get_id(path.USB1))
    crypt.write_key(master_key, path_crypted_mk)

if __name__ == "__main__":
    main()
