# Rendu TP3 "Un fichier de carte bancaires"  

## Binome  

- Decamps, Alexandre, email: alexandre.decamps.etu@univ-lille.fr  

- Bédart, Cyprien, email: cyprien.bedart.etu@univ-lille.fr  

## Partie 1 : "Responsabilité partagée"  

### Q1  

Init :  
- Deux clés USB contenant chacune une clé cryptée dont la clé de décryptage est un mot de passe.  
- La Master Key cryptée sur le serveur avec pour seul moyen de la décrypter la fusion des deux clés présentes sur les clés USB une fois décryptée.  

Fonctionnement :  

Chacun des deux responsables possède une clé cryptée sur une clé USB dont la clé de décryptage est un mot de passe connu uniquement par le propriétaire de la clé.  
Une fois que ces deux clés sont décryptées, une utilisation successives de ces des deux clés  va permettre de décrypter la Master Key présente sur le serveur.  
Cette Master Key décryptée va être ensuite sauvegardée sur la RAM et va servir à débloquer les informations de carte présentes sur le serveur.  


### Q2  

Afin d'utiliser les programmes suivant, il faut avoir python3 installé ainsi que les packages `cryptography` et `paramiko`

#### Initialisation  

L'initialisation du serveur est implémentée dans le fichier init.py présent dans le dossier src et s'exécute avec la commande suivante :  
```
python3 src/init.py
```

Par défaut, l'usb0 est celui du responsable Juridique et l'usb1 celui du responsable Technique.  

#### Lancement  

Après avoir fait l'initialisation, pour lancer le serveur, il faut lancer la commande suivante :  
```
python3 .\src\demarrage.py PATH_UBS0 PATH_USB1
```

#### Gestion des cartes  

Pour ajouter une paire nom/carte, il faut lancer :  
```
python3 .\src\add_pair.py NOM NUMERO
```

Pour supprimer une paire nom/carte, il faut lancer :  
```
python3 .\src\delete_pair.py NOM NUMERO
```

Afin de chercher toutes les cartes d'une personne, il faut lancer :  
```
python3 .\src\serch_card.py NOM
```


## Partie 2 : "Délégation de droit"  

### Q3  

Nous avons :
- Le responsable juridique R0 avec son représentant légal RL0
- Le responsable technique R1 avec son représentant légal RL1

Le but est donc de pouvoir lancer le serveur avec les couples :
- R0 - R1
- R0 - RL1
- RL0 - R1
- RL0 - RL1

Pour cela, nous allons garder sur le serveur 4 versions de la MASTER_KEY, un pour chaque couple permettant le lancement du serveur.  

### Q4  

Afin d'ajouter un réprésentant légal, il faut utiliser :  
```
python3 scripts/add_legal.py USB_PATH_NEW ROLE USB_PATH_ASO
```
Avec `USB_PATH_NEW` : Le chemin vers la nouvelle clé usb.  
Avec `ROLE` : legal ou technical.  
Avec `USB_PATH_ASOX` : Le chemin vers la clé usb qui permettra, en association avec le nouveau responsable créé, de démarrer le serveur.  

exemple :  
```
python3 .\src\add_legal.py disques/usb2 legal disks/usb1
```

## Partie 3 : "Révocation de droit"  

### Q5  

Pour faire la répudiation, nous allons demander à chaque personne de donner son couple login/mdp et si tout le monde (à l'exception de la personne à supprimer) s'est connecté alors la personne est supprimée.  

### Q6  

Afin de supprimer un responsable, il faut lancer :  
```
python3 .\src\revoc.py ID
```
